import React from 'react'
import { View, Text, Button } from 'react-native'
import { DrawerNavigator, createNavigationContainer } from 'react-navigation'
import { Constants } from 'expo'
// import { router as rootRouter } from './RootNavigator'

const MainA = () => (
  <View>
    <Text>A</Text>
    <Text>b</Text>
  </View>
)

const MainB = () => (
  <View>
    <Text>a</Text>
    <Text>B</Text>
  </View>
)

const MainC = () => (
  <View>
    <Text>C</Text>
    <Text>b</Text>
  </View>
)

const SideMenu = ({ navigation }) => (
  <View>
    <Button title="intro" onPress={() => navigation.navigate('Intro')} />
    <Button title="a" onPress={() => navigation.navigate('MainA')} />
    <Button title="b" onPress={() => navigation.navigate('MainB')} />
    <Button title="c" onPress={() => navigation.navigate('MainC')} />
  </View>
)

const MenuDrawerNavigator = DrawerNavigator(
  {
    MainA: {
      screen: MainA,
      path: 'maina',
      navigationOptions: {
        drawerLabel: () => null
      }
    },
    MainB: {
      screen: MainB,
      path: 'mainb',
      navigationOptions: {
        drawerLabel: () => null
      }
    },
    MainC: {
      screen: MainC,
      path: 'mainc',
      navigationOptions: {
        drawerLabel: () => null
      }
    }
  },
  {
    initialRouteName: 'MainA',
    contentComponent: SideMenu
  }
)

console.log('MenuDrawerNavigator keys', Object.keys(MenuDrawerNavigator))
console.log(MenuDrawerNavigator.router)
export const router = MenuDrawerNavigator.router

// const uriPrefix = Constants.linkingUri.replace(/([^+]+)\+?$/, '$1+')

// const withScreenProps = Navigator =>
//   class MenuNavigator extends React.PureComponent {
//     static router = router

//     render() {
//       return <Navigator screenProps={this.props} />
//     }
//   }

// export default withScreenProps(MenuDrawerNavigator)

export default MenuDrawerNavigator

// export default class MenuNavigator extends React.PureComponent {
//     static router = MenuDrawerNavigator.router

//   render() {
//     return (
//         // onNavigationStateChange={trackNavigationChange}
//       <MenuDrawerNavigator
//         screenProps={this.props}
//       />
//     )
//   }
// }

// export default createNavigationContainer(MenuDrawerNavigator)