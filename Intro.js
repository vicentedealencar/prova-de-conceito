import React, { Component } from 'react'
import { Text, View, TouchableOpacity, AsyncStorage } from 'react-native'

export default class ContaClique extends Component {
  state = {
    lastColor: '',
    greenCount: 0,
    blueCount: 0
  }

  saveState = state => {
    this.setState(state)
    AsyncStorage.setItem('state', JSON.stringify(state))
  }

  componentDidMount() {
    AsyncStorage.getItem('state').then(state => {
      this.setState(JSON.parse(state))
    })
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-around',
          alignItems: 'center'
        }}
      >
        <BallButton
          backgroundColor="#77ee77"
          title={this.state.greenCount.toString()}
          onPress={() =>
            this.saveState({
              greenCount: this.state.greenCount + 1,
              blueCount: this.state.blueCount - 1,
              lastColor: 'verde'
            })
          }
        />
        <Text>{this.state.lastColor}</Text>
        <BallButton
          backgroundColor="#0000ee"
          color="#ffffff"
          title={this.state.blueCount.toString()}
          onPress={() =>
            this.saveState({
              greenCount: this.state.greenCount - 1,
              blueCount: this.state.blueCount + 1,
              lastColor: 'azul'
            })
          }
        />
      </View>
    )
  }
}

const BallButton = ({ onPress, title, backgroundColor, color }) => (
  <TouchableOpacity onPress={onPress}>
    <View
      style={{
        width: 100,
        height: 100,
        backgroundColor,
        borderRadius: 100 / 2,
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <Text style={{ color }}>{title}</Text>
    </View>
  </TouchableOpacity>
)
