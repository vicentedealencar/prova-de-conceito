import React from 'react'
import { StackNavigator } from 'react-navigation'
import { Constants } from 'expo'

import Main from './Main'
import Intro from './Intro'

export const RootStackNavigator = StackNavigator(
  {
    Intro: {
      screen: Intro,
      path: 'intro'
    },
    Main: {
      screen: Main,
      path: 'main'
    }
  },
  {
    initialRouteName: 'Intro'
  }
)


console.log('RootStackNavigator keys', Object.keys(RootStackNavigator))
console.log(RootStackNavigator.router)
export const router = RootStackNavigator.router


const uriPrefix = Constants.linkingUri.replace(/([^+]+)\+?$/, '$1+') + '/'
// const withDeeplinking = Navigator =>
//   class RootNavigator extends React.PureComponent {
//     render() {
//       console.log('uriPrefix', uriPrefix)
//       return <Navigator screenProps={this.props} uriPrefix={uriPrefix} />
//     }
//   }

// export default withDeeplinking(RootStackNavigator)

export default class RootNavigator extends React.PureComponent {
  render() {
    return <RootStackNavigator screenProps={this.props} uriPrefix={uriPrefix} />
  }
}
